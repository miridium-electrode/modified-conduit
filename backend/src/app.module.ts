import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { GraphQLModule } from '@nestjs/graphql';
import { AuthModule } from './auth/auth.module';
import { join } from 'path';
import { ArticleModule } from './article/article.module';
import { ErrorModule } from './error/error.module';
import { ConfigModule } from '@nestjs/config';
import { ImagesModule } from './images/images.module';
import { TagsModule } from './tags/tags.module';
import { ProfileModule } from './profile/profile.module';
import { UserModule } from './user/user.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      typePaths: [join(process.cwd(), '../common/*.graphql')],
      cors: {
        origin: 'http://localhost:3000',
        credentials: true,
      },
      context: ({ req, res }) => ({ req, res }),
      playground: {
        settings: {
          'request.credentials': 'include',
        },
      },
      // installSubscriptionHandlers: true
    }),
    AuthModule,
    ArticleModule,
    ErrorModule,
    ImagesModule,
    TagsModule,
    ProfileModule,
    UserModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
