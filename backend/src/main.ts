import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { PrismService } from './prism/prism.service';
import * as cookieParser from 'cookie-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const prismaService = app.get(PrismService);
  await prismaService.enableShutdownHooks(app);
  app.use(cookieParser());
  await app.listen(8000);
}
bootstrap();
