import { UseGuards } from '@nestjs/common';
import { Args, Query, Resolver, Mutation } from '@nestjs/graphql';
import { OptionalGuard } from 'src/auth/optional.guard';
import { ProfileService } from './profile.service';
import { Auth as User } from 'src/auth/auth.decorator';
import { JwtGuard } from 'src/auth/jwt.guard';

@Resolver()
export class ProfileResolver {
  constructor(private service: ProfileService) {}

  @Query('getProfile')
  @UseGuards(OptionalGuard)
  async getProfile(
    @Args('username') username: string,
    @User() user?: { userId: string; username: string },
  ) {
    return this.service.getProfile(username, user?.username);
  }

  @Mutation('followUser')
  @UseGuards(JwtGuard)
  async followUser(
    @Args('username') username: string,
    @User() loggedIn: { userId: string; username: string },
  ) {
    return this.service.followUser(username, loggedIn.username);
  }

  @Mutation('unfollowUser')
  @UseGuards(JwtGuard)
  async unfollowUser(
    @Args('username') username: string,
    @User() loggedIn: { userId: string; username: string },
  ) {
    return this.service.unfollowUser(username, loggedIn.username);
  }
}
