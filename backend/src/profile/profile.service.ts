import { Injectable } from '@nestjs/common';
import { PrismService } from 'src/prism/prism.service';
import { Profile as ProfileGql } from 'src/generated.graphql';
import { Prisma } from '@prisma/client';

type PrismaUsersAndFollowers = Prisma.UsersGetPayload<{
  where: {
    username;
  };
  include: {
    followed: {
      select: {
        follower: {
          select: {
            username: true;
          };
        };
      };
    };
  };
}>;

type PrismaProfileFollowers = {
  follower: {
    username: string;
  };
}[];

@Injectable()
export class ProfileService {
  constructor(private db: PrismService) {}

  async getProfile(
    username: string,
    currentUser?: string,
  ): Promise<ProfileGql> {
    try {
      const user = await this.db.users.findUniqueOrThrow({
        where: {
          username,
        },
        include: {
          followed: {
            select: {
              follower: {
                select: {
                  username: true,
                },
              },
            },
          },
        },
      });

      return {
        username: user.username,
        bio: user.bio,
        image: user.image,
        following: this.isFollowing(user.followed, currentUser),
      };
    } catch (error) {
      throw error;
    }
  }

  async followUser(username: string, loggedInUser: string) {
    try {
      const user = await this.db.users.findUniqueOrThrow({
        where: {
          username: username,
        },
      });

      const follower = await this.db.users.findUniqueOrThrow({
        where: {
          username: loggedInUser,
        },
      });

      await this.db.followings.create({
        data: {
          followed_id: user.id,
          follower_id: follower.id,
        },
      });

      const author = await this.db.users.findUniqueOrThrow({
        where: {
          username,
        },
        include: {
          followed: {
            select: {
              follower: {
                select: {
                  username: true,
                },
              },
            },
          },
        },
      });

      return this.toProfileGql(author, loggedInUser);
    } catch (error) {
      throw error;
    }
  }

  async unfollowUser(username: string, loggedInUser: string) {
    try {
      const user = await this.db.users.findUnique({
        where: {
          username,
        },
      });

      const follower = await this.db.users.findUnique({
        where: {
          username: loggedInUser,
        },
      });

      if (user && follower) {
        const oldFollow = await this.db.followings.findFirstOrThrow({
          where: {
            AND: [
              {
                followed_id: user.id,
              },
              {
                follower_id: follower.id,
              },
            ],
          },
        });
        await this.db.followings.delete({
          where: {
            id: oldFollow.id,
          },
        });
      }

      const author = await this.db.users.findUniqueOrThrow({
        where: {
          username,
        },
        include: {
          followed: {
            select: {
              follower: {
                select: {
                  username: true,
                },
              },
            },
          },
        },
      });

      return this.toProfileGql(author, loggedInUser);
    } catch (error) {
      throw error;
    }
  }

  private isFollowing(followers: PrismaProfileFollowers, currentUser?: string) {
    if (!currentUser) {
      return false;
    }

    const isFollowed = followers.find(
      (f) => f.follower.username === currentUser,
    );
    if (isFollowed === undefined) {
      return false;
    } else {
      return true;
    }
  }

  private toProfileGql(
    usersPrisma: PrismaUsersAndFollowers,
    loggedInUser?: string,
  ): ProfileGql {
    let profile = new ProfileGql();
    profile.username = usersPrisma.username;
    profile.bio = usersPrisma.bio;
    profile.image = usersPrisma.image;
    profile.following = this.isFollowing(usersPrisma.followed, loggedInUser);
    return profile;
  }
}
