import { Module } from '@nestjs/common';
import { AuthModule } from 'src/auth/auth.module';
import { OptionalGuard } from 'src/auth/optional.guard';
import { PrismModule } from 'src/prism/prism.module';
import { PrismService } from 'src/prism/prism.service';
import { ProfileResolver } from './profile.resolver';
import { ProfileService } from './profile.service';
import { JwtGuard } from 'src/auth/jwt.guard';

@Module({
  imports: [PrismModule, AuthModule],
  providers: [
    ProfileResolver,
    ProfileService,
    PrismService,
    OptionalGuard,
    JwtGuard,
  ],
})
export class ProfileModule {}
