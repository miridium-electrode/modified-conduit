import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime/library';

@Injectable()
export class ErrorService {
  resolverInternalError(error: any) {
    if (error instanceof PrismaClientKnownRequestError) {
      console.error(`${error.code}: ${error.message}`);
    } else {
      console.error(error);
    }
    throw new InternalServerErrorException();
  }
}
