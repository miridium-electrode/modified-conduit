import { Test, TestingModule } from '@nestjs/testing';
import { ArticleService } from './article.service';
import { PrismService } from '../prism/prism.service';
import { mockDeep, DeepMockProxy } from 'jest-mock-extended';

describe('ArticleService without tags', () => {
  type ArticlePrismMixin = {
    slug: string;
    title: string;
    description: string;
    body: string;
    tags: any[];
    created_at: string;
    updated_at: string;
    author: {
      email: string;
      username: string;
      bio: string;
      image: string;
    };
  };

  type ArticlePrism = {
    create: ArticlePrismMixin;
    findMany: ArticlePrismMixin[];
  };
  const dummyDate = new Date().toISOString();
  const articlePrism = {
    create: {
      slug: 'how-to-create-navbar-using-tailwind',
      title: 'How to create navbar using tailwind',
      description: 'tailwind navbar cool',
      body: 'tailwind navbar very cool',
      tags: [],
      created_at: dummyDate,
      updated_at: dummyDate,
      author: {
        email: 'tessia@gmail.com',
        username: 'tessia',
        bio: '',
        image: '',
      },
    },
  };
  let service: ArticleService;
  let db: DeepMockProxy<PrismService>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticleService,
        { provide: PrismService, useValue: mockDeep<PrismService>() },
      ],
    }).compile();

    service = module.get<ArticleService>(ArticleService);
    db = module.get(PrismService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create article without any tags', async () => {
    const returnedArticle = {
      slug: 'how-to-create-navbar-using-tailwind',
      title: 'How to create navbar using tailwind',
      description: 'tailwind navbar cool',
      body: 'tailwind navbar very cool',
      tagList: [],
      createdAt: dummyDate,
      updatedAt: dummyDate,
      favorited: false,
      favoritesCount: 0,
      author: {
        email: 'tessia@gmail.com',
        username: 'tessia',
        bio: '',
        image: '',
      },
    };

    const payload = {
      title: 'How to create navbar using tailwind',
      description: 'tailwind navbar cool',
      body: 'tailwind navbar very cool',
      tagList: [],
    };

    const authorId = 1;

    // @ts-ignore
    db.articles.create.mockResolvedValueOnce(articlePrism.create);

    db.articles.create.mock.calls;

    let createdArticle = await service.create(payload, authorId);

    expect(createdArticle).toEqual(returnedArticle);
  });
});
