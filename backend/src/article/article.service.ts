import { Injectable } from '@nestjs/common';
import { PrismService } from '../prism/prism.service';
import { CreateArticle, UpdateArticle } from './article.dto';
import { Favorites, Prisma } from '@prisma/client';
import {
  Articles as ArticlesGql,
  Article as ArticleGql,
  Author as AuthorGql,
} from '../generated.graphql';

type PrismaSelectedArticle = Prisma.ArticlesGetPayload<{
  include: {
    tags: {
      select: {
        tag: {
          select: {
            name: true;
          };
        };
      };
    };
    author: {
      select: {
        bio: true;
        username: true;
        image: true;
        followed: {
          select: {
            follower: {
              select: {
                username: true;
              };
            };
          };
        };
      };
    };
    favoritedBy: {
      select: {
        user: {
          select: {
            username: true;
          };
        };
      };
    };
  };
}>;

type AllArticles = PrismaSelectedArticle[];

type PrismaSelectedTags = {
  tag: {
    name: string;
  };
}[];

type PrismaSelectedFavoritedBy = {
  user: {
    username: string;
  };
}[];

type PrismaSelectedFollowers = {
  follower: {
    username: string;
  };
}[];

@Injectable()
export class ArticleService {
  constructor(private db: PrismService) {}

  async create(payload: CreateArticle, authorId: number): Promise<ArticleGql> {
    try {
      const slug = payload.title.replace(' ', '-');
      const sameDate = new Date().toISOString();
      const newArticle = await this.db.articles.create({
        data: {
          body: payload.body,
          created_at: sameDate,
          description: payload.description,
          slug,
          title: payload.title,
          updated_at: sameDate,
          author: {
            connect: {
              id: authorId,
            },
          },
        },
        select: {
          id: true,
          slug: true,
          title: true,
          description: true,
          body: true,
          tags: {
            select: {
              tag: {
                select: {
                  name: true,
                },
              },
            },
          },
          created_at: true,
          updated_at: true,
          author: {
            select: {
              bio: true,
              username: true,
              image: true,
            },
          },
        },
      });

      return {
        author: {
          following: false,
          username: newArticle.author.username,
          bio: newArticle.author.bio,
          image: newArticle.author.bio,
        },
        body: newArticle.body,
        createdAt: newArticle.created_at.toDateString(),
        description: newArticle.description,
        slug: newArticle.slug,
        title: newArticle.title,
        updatedAt: newArticle.updated_at.toDateString(),
        favorited: false,
        favoritesCount: 0,
        tagList: payload.tagList,
        id: newArticle.id,
      };
    } catch (error) {
      throw error;
    }
  }

  // async updateArticle(
  //   id: number,
  //   payload: UpdateArticle,
  //   user: { userId: string; username: string },
  // ): Promise<ArticleGql> {
  //   try {
  //     await this.db.users.findUniqueOrThrow({
  //       where: {
  //         id: Number(user.userId),
  //       },
  //     });

  //     const oldArticle = await this.db.articles.findUniqueOrThrow({
  //       where: {
  //         id,
  //       },
  //     });

  //     const newArticle = await this.db.articles.update({
  //       where: {
  //         id,
  //       },
  //       data: {
  //         title: payload.title ?? oldArticle.title,
  //         description: payload.description ?? oldArticle.description,
  //         body: payload.description ?? oldArticle.description,
  //       },
  //     });

  //     return;
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  async getArticles(
    count?: number,
    page?: number,
    tags?: string[],
    author?: string,
    favorited?: boolean,
    currentUser?: { userId: string; username: string },
  ): Promise<ArticlesGql> {
    try {
      const validPage = page !== undefined || page > 0 ? page : 1;
      const validCount = count !== undefined || count >= 10 ? count : 10;
      const endPage = validPage * validCount;
      const beginPage = endPage - validCount;

      const articles = await this.db.articles.findMany({
        include: {
          tags: {
            select: {
              tag: {
                select: {
                  name: true,
                },
              },
            },
          },
          author: {
            select: {
              bio: true,
              username: true,
              image: true,
              followed: {
                select: {
                  follower: {
                    select: {
                      username: true,
                    },
                  },
                },
              },
            },
          },
          favoritedBy: {
            select: {
              user: {
                select: {
                  username: true,
                },
              },
            },
          },
        },
        orderBy: {
          updated_at: 'desc',
        },
      });

      let filteredArticle: AllArticles = [...articles];

      if (tags !== undefined) {
        if (tags.length > 0) {
          filteredArticle = this.filterByTags(filteredArticle, tags);
        }
      }

      if (author !== undefined) {
        filteredArticle = this.filterByAuthorName(filteredArticle, author);
      }

      if (favorited !== undefined && currentUser !== undefined) {
        filteredArticle = this.filterByFavorited(
          filteredArticle,
          currentUser.username,
        );
      }

      if (currentUser) {
        return this.transformToGraphqlArticlesResponse(
          filteredArticle,
          beginPage,
          endPage,
          currentUser.username,
        );
      } else {
        return this.transformToGraphqlArticlesResponse(
          filteredArticle,
          beginPage,
          endPage,
        );
      }
    } catch (error) {
      throw error;
    }
  }

  async getArticlesBySlug(
    slug: string,
    currentUser?: { userId: string; username: string },
  ): Promise<ArticleGql> {
    try {
      const article = await this.db.articles.findUniqueOrThrow({
        where: {
          slug,
        },
        include: {
          tags: {
            select: {
              tag: {
                select: {
                  name: true,
                },
              },
            },
          },
          author: {
            select: {
              bio: true,
              username: true,
              image: true,
              followed: {
                select: {
                  follower: {
                    select: {
                      username: true,
                    },
                  },
                },
              },
            },
          },
          favoritedBy: {
            select: {
              user: {
                select: {
                  username: true,
                },
              },
            },
          },
        },
      });

      if (currentUser) {
        return this.transformToGraphqlArticleResponse(
          article,
          currentUser.username,
        );
      } else {
        return this.transformToGraphqlArticleResponse(article);
      }
    } catch (error) {
      throw error;
    }
  }

  async favoriteArticle(
    slug: string,
    currentUser: string,
  ): Promise<ArticleGql> {
    try {
      const user = await this.db.users.findUniqueOrThrow({
        where: {
          username: currentUser,
        },
      });

      const article = await this.db.articles.findUniqueOrThrow({
        where: {
          slug,
        },
      });

      await this.db.favorites.create({
        data: {
          article_id: article.id,
          user_id: user.id,
        },
      });

      const likedArticle = await this.db.articles.findUniqueOrThrow({
        where: {
          slug,
        },
        include: {
          author: {
            select: {
              username: true,
              bio: true,
              image: true,
              followed: {
                select: {
                  follower: {
                    select: {
                      username: true,
                    },
                  },
                },
              },
            },
          },
          tags: {
            select: {
              tag: {
                select: {
                  name: true,
                },
              },
            },
          },
          favoritedBy: {
            select: {
              user: {
                select: {
                  username: true,
                },
              },
            },
          },
        },
      });

      return this.transformToGraphqlArticleResponse(likedArticle, currentUser);
    } catch (error) {
      throw error;
    }
  }

  async unfavoriteArticle(slug: string, currentUser): Promise<ArticleGql> {
    try {
      const article = await this.db.articles.findUnique({
        where: {
          slug,
        },
      });

      const user = await this.db.users.findUnique({
        where: {
          username: currentUser,
        },
      });

      if (user !== null && article !== null) {
        const oldFavorite = await this.db.favorites.findFirst({
          where: {
            user_id: user.id,
            article_id: article.id,
          },
        });

        await this.db.favorites.delete({
          where: {
            id: oldFavorite.id,
          },
        });
      }

      const unlikedArticle = await this.db.articles.findUniqueOrThrow({
        where: {
          slug,
        },
        include: {
          author: {
            select: {
              username: true,
              bio: true,
              image: true,
              followed: {
                select: {
                  follower: {
                    select: {
                      username: true,
                    },
                  },
                },
              },
            },
          },
          tags: {
            select: {
              tag: {
                select: {
                  name: true,
                },
              },
            },
          },
          favoritedBy: {
            select: {
              user: {
                select: {
                  username: true,
                },
              },
            },
          },
        },
      });

      return this.transformToGraphqlArticleResponse(
        unlikedArticle,
        currentUser,
      );
    } catch (error) {
      throw error;
    }
  }

  private filterByTags(articles: AllArticles, tags: string[]) {
    let taggedArticle: AllArticles = [];

    for (let tag of tags) {
      if (taggedArticle.length === 0) {
        taggedArticle = this.filterByTag(articles, tag);
      } else {
        taggedArticle = this.filterByTag(taggedArticle, tag);
      }
    }

    return taggedArticle;
  }

  private filterByTag(articles: AllArticles, tag: string) {
    const nonEmptyTaggedArticles = articles.filter(
      (article) => article.tags.length !== 0,
    );

    let taggedArticles: AllArticles = [];

    for (let nonEmptyTaggedArticle of nonEmptyTaggedArticles) {
      for (let tags of nonEmptyTaggedArticle.tags) {
        if (tags.tag.name === tag) {
          taggedArticles.push(nonEmptyTaggedArticle);
        }
      }
    }

    return taggedArticles;
  }

  private filterByAuthorName(articles: AllArticles, name: string) {
    return articles.filter((article) => article.author.username === name);
  }

  private filterByFavorited(articles: AllArticles, currentUser: string) {
    const filteredArticles: AllArticles = [];

    for (const article of articles) {
      for (const favorited of article.favoritedBy) {
        if (favorited.user.username === currentUser) {
          filteredArticles.push(article);
        }
      }
    }

    return filteredArticles;
  }

  private toTagList(tags: PrismaSelectedTags): string[] {
    return tags.map((tags) => tags.tag.name);
  }

  private favoritesTransform(
    favoritedBy: PrismaSelectedFavoritedBy,
    currentUser?: string,
  ): {
    favorited: boolean;
    favoritesCount: number;
  } {
    if (currentUser === undefined) {
      return {
        favorited: false,
        favoritesCount: favoritedBy.length,
      };
    }

    const isFavorited = favoritedBy.filter(
      (fv) => fv.user.username === currentUser,
    );

    if (isFavorited.length === 0) {
      return {
        favorited: false,
        favoritesCount: favoritedBy.length,
      };
    } else {
      return {
        favorited: true,
        favoritesCount: favoritedBy.length,
      };
    }
  }

  private isFollowed(
    prismaFollowers: PrismaSelectedFollowers,
    currentUser?: string,
  ): boolean {
    if (currentUser === undefined) {
      return false;
    }

    let isFollowedByCurrentUser = prismaFollowers.find(
      (followed) => followed.follower.username === currentUser,
    );
    if (isFollowedByCurrentUser) {
      return true;
    } else {
      return false;
    }
  }

  private transformToGraphqlArticlesResponse(
    articles: AllArticles,
    begin: number,
    end: number,
    currentUser?: string,
  ): ArticlesGql {
    let articlesGql: ArticleGql[] = [];
    for (let article of articles) {
      let articleGql = this.transformToGraphqlArticleResponse(
        article,
        currentUser,
      );

      articlesGql.push(articleGql);
    }

    const slicedArticles = articlesGql.slice(begin, end);

    return {
      articlesCount: articlesGql.length,
      articles: slicedArticles,
    };
  }

  private transformToGraphqlArticleResponse(
    article: PrismaSelectedArticle,
    currentUser?: string,
  ): ArticleGql {
    let authorGql = new AuthorGql();
    authorGql.username = article.author.username;
    authorGql.bio = article.author.bio;
    authorGql.image = article.author.image;
    authorGql.following = this.isFollowed(article.author.followed, currentUser);

    let favorites = this.favoritesTransform(article.favoritedBy, currentUser);

    let articleGql = new ArticleGql();
    articleGql.author = authorGql;
    articleGql.body = article.body;
    articleGql.createdAt = article.created_at.toDateString();
    articleGql.description = article.description;
    articleGql.favorited = favorites.favorited;
    articleGql.favoritesCount = favorites.favoritesCount;
    articleGql.slug = article.slug;
    articleGql.tagList = this.toTagList(article.tags);
    articleGql.title = article.title;
    articleGql.updatedAt = article.updated_at.toDateString();
    articleGql.id = article.id;

    return articleGql;
  }
}
