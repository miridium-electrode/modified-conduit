import { Module } from '@nestjs/common';
import { PrismModule } from 'src/prism/prism.module';
import { PrismService } from 'src/prism/prism.service';
import { ArticleService } from './article.service';
import { ArticleResolver } from './article.resolver';
import { JwtGuard } from 'src/auth/jwt.guard';
import { AuthModule } from 'src/auth/auth.module';
import { ErrorModule } from 'src/error/error.module';
import { ErrorService } from 'src/error/error.service';
import { OptionalGuard } from 'src/auth/optional.guard';

@Module({
  providers: [
    ArticleService,
    PrismService,
    ArticleResolver,
    JwtGuard,
    ErrorService,
    OptionalGuard,
  ],
  imports: [PrismModule, AuthModule, ErrorModule],
})
export class ArticleModule {}
