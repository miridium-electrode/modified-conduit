export class CreateArticle {
  title: string;
  description: string;
  body: string;
  tagList?: string[];
}

export class UpdateArticle {
  title?: string;
  description?: string;
  body?: string;
  tagList?: string[];
}
