import { Args, Mutation, Resolver, Query, Context } from '@nestjs/graphql';
import { ArticleService } from './article.service';
import { Auth as User } from 'src/auth/auth.decorator';
import { UseGuards } from '@nestjs/common';
import { JwtGuard } from 'src/auth/jwt.guard';
import { ErrorService } from 'src/error/error.service';
import { OptionalGuard } from 'src/auth/optional.guard';

@Resolver()
export class ArticleResolver {
  constructor(
    private articleService: ArticleService,
    private errorS: ErrorService,
  ) {}

  @Mutation('createArticle')
  @UseGuards(JwtGuard)
  async createArticle(
    @Args('title') title: string,
    @Args('description') description: string,
    @Args('body') body: string,
    @Args('tagList') tagList: string[],
    @User() user: { userId: string; username: string },
  ) {
    try {
      return this.articleService.create(
        {
          body,
          description,
          title,
          tagList,
        },
        Number(user.userId),
      );
    } catch (error) {
      this.errorS.resolverInternalError(error);
    }
  }

  // @Mutation('updateArticle')
  // @UseGuards(JwtGuard)
  // async updateArticle(
  //   @Args('id') id: number,
  //   @User() user: { userId: string; username: string },
  //   @Args('title') title?: string,
  //   @Args('description') description?: string,
  //   @Args('body') body?: string,
  //   @Args('tagList') tagList?: string[],
  // ) {
  //   return this.articleService.updateArticle(
  //     id,
  //     {
  //       body,
  //       description,
  //       tagList,
  //       title,
  //     },
  //     user,
  //   );
  // }

  @Query('getArticles')
  @UseGuards(OptionalGuard)
  async getArticles(
    @Args('count') count?: number,
    @Args('page') page?: number,
    @Args('tags') tags?: string[],
    @Args('author') author?: string,
    @Args('favorited') favorited?: boolean,
    @User() user?: { userId: string; username: string },
  ) {
    try {
      return this.articleService.getArticles(
        count,
        page,
        tags,
        author,
        favorited,
        user,
      );
    } catch (error) {
      this.errorS.resolverInternalError(error);
    }
  }

  @Query('getArticleBySlug')
  @UseGuards(OptionalGuard)
  async getArticleBySlug(
    @Args('slug') slug: string,
    @User() user?: { userId: string; username: string },
  ) {
    try {
      return this.articleService.getArticlesBySlug(slug, user);
    } catch (error) {
      this.errorS.resolverInternalError(error);
    }
  }

  @Mutation('favoriteArticle')
  @UseGuards(JwtGuard)
  async favoriteArticle(
    @Args('slug') slug: string,
    @User() user: { userId: string; username: string },
  ) {
    return this.articleService.favoriteArticle(slug, user.username);
  }

  @Mutation('unfavoriteArticle')
  @UseGuards(JwtGuard)
  async unfavoriteArticle(
    @Args('slug') slug: string,
    @User() user: { userId: string; username: string },
  ) {
    return this.articleService.unfavoriteArticle(slug, user.username);
  }
}
