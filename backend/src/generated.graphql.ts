
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */

export class User {
    email: string;
    token: string;
    username: string;
    bio?: Nullable<string>;
    image?: Nullable<string>;
}

export class Profile {
    username: string;
    bio?: Nullable<string>;
    image?: Nullable<string>;
    following: boolean;
}

export class Author {
    following: boolean;
    username: string;
    bio?: Nullable<string>;
    image?: Nullable<string>;
}

export class Article {
    id: number;
    slug: string;
    title: string;
    description: string;
    body: string;
    tagList?: Nullable<string[]>;
    createdAt: string;
    updatedAt: string;
    favorited: boolean;
    favoritesCount: number;
    author: Author;
}

export class Articles {
    articles?: Nullable<Article[]>;
    articlesCount: number;
}

export class Comment {
    id: number;
    createdAt: string;
    updatedAt: string;
    body: string;
    author: Author;
}

export class Comments {
    comments?: Nullable<Comment[]>;
}

export class Tags {
    tags?: Nullable<string[]>;
}

export abstract class IQuery {
    abstract getTags(): Nullable<Tags> | Promise<Nullable<Tags>>;

    abstract getArticles(count?: Nullable<number>, page?: Nullable<number>, tags?: Nullable<string[]>, author?: Nullable<string>, favorited?: Nullable<boolean>): Articles | Promise<Articles>;

    abstract getArticleBySlug(slug: string): Article | Promise<Article>;

    abstract getProfile(username: string): Profile | Promise<Profile>;

    abstract getCurrentUser(): User | Promise<User>;
}

export abstract class IMutation {
    abstract login(email: string, password: string): User | Promise<User>;

    abstract register(username: string, email: string, password: string, repass: string): User | Promise<User>;

    abstract createArticle(title: string, description: string, body: string, tagList?: Nullable<string[]>): Article | Promise<Article>;

    abstract favoriteArticle(slug: string): Article | Promise<Article>;

    abstract unfavoriteArticle(slug: string): Article | Promise<Article>;

    abstract followUser(username: string): Profile | Promise<Profile>;

    abstract unfollowUser(username: string): Profile | Promise<Profile>;

    abstract updateCurrentUser(username: string, email?: Nullable<string>, oldpass?: Nullable<string>, newpass?: Nullable<string>, image?: Nullable<string>, bio?: Nullable<string>): User | Promise<User>;
}

type Nullable<T> = T | null;
