import { Module } from '@nestjs/common';
import { PrismService } from './prism.service';

@Module({
  imports: [],
  providers: [PrismService],
  exports: [PrismService]
})
export class PrismModule {}
