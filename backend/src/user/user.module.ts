import { Module } from '@nestjs/common';
import { UserResolver } from './user.resolver';
import { UserService } from './user.service';
import { PrismModule } from 'src/prism/prism.module';
import { AuthModule } from 'src/auth/auth.module';
import { PrismService } from 'src/prism/prism.service';
import { JwtGuard } from 'src/auth/jwt.guard';

@Module({
  providers: [UserResolver, UserService, PrismService, JwtGuard],
  imports: [PrismModule, AuthModule],
})
export class UserModule {}
