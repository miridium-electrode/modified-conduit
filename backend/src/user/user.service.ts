import { BadRequestException, Injectable } from '@nestjs/common';
import { PrismService } from 'src/prism/prism.service';
import { User as UserGql } from 'src/generated.graphql';
import { Users as UsersPrisma } from '@prisma/client';
import { UpdateUser as UpdateDTO } from './user.dto';
import * as argon2 from 'argon2';

@Injectable()
export class UserService {
  constructor(private db: PrismService) {}

  async getCurrentUser(username: string): Promise<UserGql> {
    try {
      const user = await this.db.users.findUniqueOrThrow({
        where: {
          username,
        },
      });

      return this.toUserGql(user);
    } catch (error) {
      throw error;
    }
  }

  async updateCurrentUser(payload: UpdateDTO) {
    try {
      if (payload.oldpass) {
        if (payload.oldpass !== payload.newpass) {
          throw new BadRequestException();
        }

        const oldPassword = await this.db.users.findUnique({
          where: {
            username: payload.username,
          },
        });

        if (oldPassword === null) {
          throw new BadRequestException();
        }

        const validOldPassword = await argon2.verify(
          oldPassword.password_hash,
          payload.oldpass,
        );

        if (!validOldPassword) {
          throw new BadRequestException();
        }

        const newPasswordHash = await argon2.hash(payload.newpass);

        await this.db.users.update({
          data: {
            password_hash: newPasswordHash,
          },
          where: {
            username: payload.username,
          },
        });
      }

      const updatedUser = await this.db.users.update({
        where: {
          username: payload.username,
        },
        data: {
          bio: payload.bio,
          email: payload.email,
          username: payload.username,
        },
      });

      return this.toUserGql(updatedUser);
    } catch (error) {}
  }

  private toUserGql(usersPrisma: UsersPrisma): UserGql {
    let user = new UserGql();
    user.username = usersPrisma.username;
    user.email = usersPrisma.email;
    user.bio = usersPrisma.bio;
    user.image = usersPrisma.image;

    return user;
  }
}
