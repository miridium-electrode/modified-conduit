export class UpdateUser {
  username: string;
  email?: string;
  oldpass?: string;
  newpass?: string;
  image?: string;
  bio?: string;
}
