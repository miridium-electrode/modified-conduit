import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UserService } from './user.service';
import { UseGuards } from '@nestjs/common';
import { Auth as User } from 'src/auth/auth.decorator';
import { JwtGuard } from 'src/auth/jwt.guard';

@Resolver()
export class UserResolver {
  constructor(private service: UserService) {}

  @Query('getCurrentUser')
  @UseGuards(JwtGuard)
  async getCurrentUser(@User() user: { userId: string; username: string }) {
    return this.service.getCurrentUser(user.username);
  }

  @Mutation('updateCurrentUser')
  @UseGuards(JwtGuard)
  async updateCurrentUser(
    @User() user: { userId: string; username: string },
    @Args('username') username: string,
    @Args('email') email?: string,
    @Args('oldpass') oldpass?: string,
    @Args('newpass') newpass?: string,
    @Args('image') image?: string,
    @Args('bio') bio?: string,
  ) {
    return this.service.updateCurrentUser({
      username,
      bio,
      email,
      image,
      newpass,
      oldpass,
    });
  }
}
