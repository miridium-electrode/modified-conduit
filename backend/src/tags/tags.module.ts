import { Module } from '@nestjs/common';
import { PrismModule } from 'src/prism/prism.module';
import { PrismService } from 'src/prism/prism.service';
import { TagsService } from './tags.service';
import { TagsResolver } from './tags.resolver';

@Module({
  imports: [PrismModule],
  providers: [TagsService, PrismService, TagsResolver],
})
export class TagsModule {}
