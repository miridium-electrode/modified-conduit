import { Query, Resolver } from '@nestjs/graphql';
import { TagsService } from './tags.service';

@Resolver()
export class TagsResolver {
  constructor(private service: TagsService) {}

  @Query('getTags')
  async getTags() {
    return this.service.getTags();
  }
}
