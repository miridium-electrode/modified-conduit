import { Injectable } from '@nestjs/common';
import { PrismService } from 'src/prism/prism.service';
import { Tags as TagsGql } from 'src/generated.graphql';

@Injectable()
export class TagsService {
  constructor(private db: PrismService) {}

  async getTags(): Promise<TagsGql> {
    try {
      const tags = await this.db.tags.findMany();

      return {
        tags: tags.map((tag) => tag.name),
      };
    } catch (error) {
      throw error;
    }
  }
}
