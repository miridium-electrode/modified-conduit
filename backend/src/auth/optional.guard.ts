import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';

@Injectable()
export class OptionalGuard implements CanActivate {
  constructor(private jwt: JwtService) {}

  async canActivate(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context);
    const req: Request = ctx.getContext().req;
    const token = req.cookies['token'];
    if (!token) {
      return true;
    }

    const payload = await this.jwt.verifyAsync(token, {
      secret: process.env.JWT_SECRET,
    });

    req['user'] = payload;

    return true;
  }
}
