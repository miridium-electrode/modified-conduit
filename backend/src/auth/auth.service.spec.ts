import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { PrismService } from '../prism/prism.service';
import { AuthService } from './auth.service';

const prismMock = {};
const jwtMock = {};

describe('AuthService', () => {
  let service: AuthService;
  let db: PrismService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: PrismService,
          useValue: prismMock,
        },
        {
          provide: JwtService,
          useValue: jwtMock,
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
    db = module.get(PrismService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
