class RegisterInnerUser {
  username: string;
  email: string;
  password: string;
  repass: string;
}

class LoginInnerUser {
  email: string;
  password: string;
}

export class LoginRequest {
  user: LoginInnerUser;
}

export class RegisterRequest {
  user: RegisterInnerUser;
}
