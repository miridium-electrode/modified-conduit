import { Args, Mutation, Resolver, Context } from '@nestjs/graphql';
import { ErrorService } from 'src/error/error.service';
import { AuthService } from './auth.service';

@Resolver()
export class AuthResolver {
  constructor(private service: AuthService) {}

  @Mutation('register')
  async register(
    @Args('username') username: string,
    @Args('email') email: string,
    @Args('password') password: string,
    @Args('repass') repass: string,
  ) {
    try {
      return this.service.register({
        user: {
          email,
          username,
          password,
          repass,
        },
      });
    } catch (error) {
      const handler = new ErrorService();
      handler.resolverInternalError(error);
    }
  }

  @Mutation('login')
  async login(
    @Args('email') email: string,
    @Args('password') password: string,
    @Context() context: any,
  ) {
    const result = await this.service.login({
      user: {
        email,
        password,
      },
    });

    context.res.cookie('token', result.token, { maxAge: 86400000 });

    return result;
  }
}
