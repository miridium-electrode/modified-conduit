import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { PrismModule } from 'src/prism/prism.module';
import { PrismService } from 'src/prism/prism.service';
import { AuthService } from './auth.service';
import { AuthResolver } from './auth.resolver';
import { JwtGuard } from './jwt.guard';
import { ErrorModule } from 'src/error/error.module';
import { ErrorService } from 'src/error/error.service';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';
import { OptionalGuard } from './optional.guard';

@Module({
  imports: [
    PrismModule,
    PassportModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '2h' },
      global: true,
    }),
    ErrorModule,
  ],
  providers: [
    AuthService,
    PrismService,
    AuthResolver,
    JwtGuard,
    ErrorService,
    JwtStrategy,
    OptionalGuard,
  ],
  exports: [JwtGuard, OptionalGuard],
})
export class AuthModule {}
