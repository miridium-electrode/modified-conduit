import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { PrismService } from '../prism/prism.service';
import { LoginRequest, RegisterRequest } from './auth.dto';
import { hash, verify } from 'argon2';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(private db: PrismService, private jwt: JwtService) {}

  async register(user: RegisterRequest) {
    try {
      const pwd = user.user.password;

      if (pwd !== user.user.repass) {
        throw new BadRequestException();
      }

      const password_hash = await hash(pwd);

      const newUser = await this.db.users.create({
        data: {
          email: user.user.email,
          password_hash,
          username: user.user.username,
        },
      });

      return {
        email: newUser.email,
        username: newUser.username,
        bio: newUser.bio,
        image: newUser.image,
        token: this.jwt.sign({ sub: newUser.id, username: newUser.username }),
      };
    } catch (error) {
      throw error;
    }
  }

  async login(user: LoginRequest) {
    try {
      const existingUser = await this.db.users.findUnique({
        where: {
          email: user.user.email,
        },
      });

      if (!existingUser) {
        throw new NotFoundException();
      }

      const validPassword = await verify(
        existingUser.password_hash,
        user.user.password,
      );

      if (!validPassword) {
        throw new UnauthorizedException();
      }

      return {
        email: existingUser.email,
        username: existingUser.username,
        bio: existingUser.bio,
        image: existingUser.image,
        token: this.jwt.sign(
          {
            sub: existingUser.id,
            username: existingUser.username,
          },
          {
            secret: process.env.JWT_SECRET,
          },
        ),
      };
    } catch (error) {
      throw error;
    }
  }
}
