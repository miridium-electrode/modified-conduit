import { Injectable } from '@nestjs/common';
import { createReadStream } from 'fs';
import { join } from 'path';
import { cwd } from 'process';

@Injectable()
export class ImagesService {
  getAvatar(username?: string) {
    return createReadStream(join(cwd(), 'storage/novatar.jpg'));
  }
}
