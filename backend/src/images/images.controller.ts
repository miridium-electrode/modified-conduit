import { Controller, Get, Header, Query, StreamableFile } from '@nestjs/common';
import { ImagesService } from './images.service';

@Controller('images')
export class ImagesController {
  constructor(private imagesService: ImagesService) {}

  @Get('avatar')
  @Header('Content-Type', 'image/jpeg')
  getAvatar(@Query('username') username?: string) {
    return new StreamableFile(this.imagesService.getAvatar());
  }
}
