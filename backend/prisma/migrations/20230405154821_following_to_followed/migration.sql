/*
  Warnings:

  - You are about to drop the column `following_id` on the `Followings` table. All the data in the column will be lost.
  - Added the required column `followed_id` to the `Followings` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "Followings" DROP CONSTRAINT "Followings_following_id_fkey";

-- AlterTable
ALTER TABLE "Followings" DROP COLUMN "following_id",
ADD COLUMN     "followed_id" INTEGER NOT NULL;

-- AddForeignKey
ALTER TABLE "Followings" ADD CONSTRAINT "Followings_followed_id_fkey" FOREIGN KEY ("followed_id") REFERENCES "Users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
