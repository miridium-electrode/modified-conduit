import { GraphQLDefinitionsFactory } from '@nestjs/graphql';
import { join } from 'path';

const definitionsFactory = new GraphQLDefinitionsFactory();
definitionsFactory.generate({
  typePaths: [join(process.cwd(), '../common/*.graphql')],
  path: join(process.cwd(), 'src/generated.graphql.ts'),
  outputAs: 'class',
});
