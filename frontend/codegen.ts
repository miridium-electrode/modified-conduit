import { CodegenConfig } from "@graphql-codegen/cli";

const config: CodegenConfig = {
  schema: `${process.cwd()}/../common/schema.graphql`,
  generates: {
    "./src/gql/": {
      overwrite: true,
      documents: ["./gqlclient/*.graphql"],
      preset: "client",
      plugins: [],
    },
  },
};

export default config;
