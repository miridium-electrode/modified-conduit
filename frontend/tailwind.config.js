/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,ts,jsx,tsx}"
  ],
  theme: {
    extend: {
      colors: {
        "article-preview-top": "rgba(0, 0, 0, 0.1)",
        "green-heart": "#22c522"
      }
    },
  },
  plugins: [],
}
