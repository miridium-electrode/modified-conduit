import { useState, useEffect } from "react";
import { TOKEN_SESSION_KEY } from "@constants";

export function useSessionToken() {
  const [token, setToken] = useState("");

  useEffect(() => {
    const sessionToken = sessionStorage.getItem(TOKEN_SESSION_KEY);
    if (sessionToken) {
      setToken(sessionToken);
    } else {
      setToken("");
    }
  }, [token]);

  return token;
}
