import { useState, useEffect } from "react";
import { LOGGED_IN_USERNAME } from "@constants";

export function useSessionUsername() {
  const [username, setUsername] = useState("");

  useEffect(() => {
    const sessionUsername = sessionStorage.getItem(LOGGED_IN_USERNAME);
    if (sessionUsername) {
      setUsername(sessionUsername);
    }
  }, [username]);

  return username;
}
