import { useRouter } from "next/router";
import { useEffect } from "react";

export function useAuthentication() {
  const router = useRouter();

  useEffect(() => {
    const token = sessionStorage.getItem("jwt");

    if (!token) {
      router.push("/login");
    }
  }, [router]);

  return true;
}
