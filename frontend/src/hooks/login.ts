import { useState, useEffect } from "react";
import { LOGGED_IN_USERNAME, TOKEN_SESSION_KEY } from "@constants";

export function useIsLoggedIn() {
  const [loggedIn, setLoggedIn] = useState(false);

  useEffect(() => {
    const username = sessionStorage.getItem(LOGGED_IN_USERNAME);
    if (username) {
      setLoggedIn(true);
    } else {
      setLoggedIn(false);
    }
  }, [loggedIn]);

  return loggedIn;
}
