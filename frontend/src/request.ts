const endpoint = "http://localhost:8000/graphql";
const avatarEndpoint = "http://127.0.0.1:8000/images/avatar";

export { endpoint, avatarEndpoint };
