import FullPage from "@components/FullPage";
import Navbar from "@components/Navbar";
import { TOKEN_SESSION_KEY } from "@constants";
import { graphql } from "@gql";
import { endpoint } from "@request";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { GraphQLClient } from "graphql-request";
import { ChangeEvent, useEffect, useState } from "react";
import { ResponseError } from "@custom-types/api-response";
import { useRouter } from "next/router";

// prettier-ignore
const currentUserDocument = graphql(`query GetCurrentUser {
  getCurrentUser {
    email
    username
    bio
    image
  }
}`)

// prettier-ignore
const updateUserDocument = graphql(`mutation UpdateCurrentUser($username: String!, $email: String, $oldpass: String, $newpass: String, $image: String, $bio: String) {
  updateCurrentUser(
    username: $username
    email: $email
    oldpass: $oldpass
    newpass: $newpass
    image: $image
    bio: $bio
  ) {
    username
    email
    bio
    image
  }
}`)

async function getUser() {
  const graphqlClient = new GraphQLClient(endpoint, { credentials: "include" });

  return graphqlClient.request(currentUserDocument);
}

type UpdateUserPayload = {
  username: string;
  email: string;
  oldpass?: string;
  newpass?: string;
  // image?: string;
  bio?: string;
};

async function updateUser(payload: UpdateUserPayload) {
  const graphqlClient = new GraphQLClient(endpoint, { credentials: "include" });

  return graphqlClient.request(updateUserDocument, {
    username: payload.username,
    bio: payload.bio,
    email: payload.email,
    // image: payload.image,
    newpass: payload.newpass,
    oldpass: payload.oldpass,
  });
}

function useSettingsData() {
  const router = useRouter();
  const { isLoading, isError, error } = useQuery({
    queryKey: ["user"],
    queryFn: getUser,
    onSuccess(data) {
      setUsername(data.getCurrentUser.username);
      setEmail(data.getCurrentUser.email);
    },
    onError(err) {
      const e = (err as any).response as ResponseError;
      if (e.errors[0].message === "Unauthorized") {
        router.replace("/login");
      }
    },
  });
  const [username, setUsername] = useState("");
  const [bio, setBio] = useState<string | undefined>();
  const [email, setEmail] = useState("");
  const [oldpass, setOldpass] = useState("");
  const [newpass, setNewpass] = useState("");

  const changeUsername = function (e: ChangeEvent<HTMLInputElement>) {
    setUsername(e.target.value);
  };

  const changeBio = function (e: ChangeEvent<HTMLTextAreaElement>) {
    setBio(e.target.value);
  };

  const changeEmail = function (e: ChangeEvent<HTMLInputElement>) {
    setEmail(e.target.value);
  };

  const changeOldpass = function (e: ChangeEvent<HTMLInputElement>) {
    setOldpass(e.target.value);
  };

  const changeNewpass = function (e: ChangeEvent<HTMLInputElement>) {
    setNewpass(e.target.value);
  };

  return {
    username,
    changeUsername,
    bio,
    changeBio,
    email,
    changeEmail,
    oldpass,
    changeOldpass,
    newpass,
    changeNewpass,
    isLoading,
    isError,
    error,
  };
}

export default function Settings() {
  const queryClient = useQueryClient();
  const {
    bio,
    changeBio,
    changeEmail,
    changeNewpass,
    changeOldpass,
    changeUsername,
    email,
    newpass,
    oldpass,
    username,
    error,
    isError,
    isLoading,
  } = useSettingsData();
  const updateMutation = useMutation({
    mutationFn: (payload: UpdateUserPayload) => updateUser(payload),
  });
  const updateButtonClick = () => {
    updateMutation.mutate(
      {
        email,
        username,
        bio,
        newpass,
        oldpass,
      },
      {
        onSuccess(data, variables, context) {
          queryClient.invalidateQueries(["user"]);
        },
      }
    );
  };

  if (isLoading) {
    return (
      <FullPage>
        <div className="grid grid-cols-12">
          <Navbar topNavMoreClass="col-span-full" />
          <p className="col-start-6 col-span-2">Loading...</p>
        </div>
      </FullPage>
    );
  }

  if (isError) {
    console.error(error);
    return (
      <FullPage>
        <div className="grid grid-cols-12">
          <Navbar topNavMoreClass="col-span-full" />
          <p className="col-start-6 col-span-2">some error occured</p>
        </div>
      </FullPage>
    );
  }

  return (
    <FullPage>
      <div className="grid grid-cols-12">
        <Navbar topNavMoreClass="col-span-full" />
        {/* settings text */}
        <div className="col-start-3 col-span-8">
          <div className="flex justify-center items-center">
            <h1 className="text-2xl font-bold">Settings</h1>
          </div>
        </div>
        {/* profile picture field */}
        <div className="col-start-3">Profile Picture</div>
        {/* username field */}
        <div className="col-start-3">Username</div>
        <input
          type="text"
          className="col-start-5 col-span-6 border border-slate-500 rounded"
          value={username}
          onChange={changeUsername}
        />
        {/* bio field */}
        <div className="col-start-3">Bio</div>
        <textarea
          cols={30}
          rows={10}
          className="col-start-5 col-span-6 border border-slate-500 rounded"
          value={bio}
          onChange={changeBio}
        ></textarea>
        {/* email field*/}
        <div className="col-start-3">Email</div>
        <input
          type="text"
          className="col-start-5 col-span-6 border border-slate-500 rounded"
          value={email}
          onChange={changeEmail}
        />
        {/* update password */}
        {/* Update Password title */}
        <div className="col-start-3 col-span-2">
          <h2 className="text-xl">Update Password</h2>
        </div>
        {/* old password field */}
        <div className="col-start-3">Old Password</div>
        <input
          type="password"
          className="col-start-5 col-span-6 border border-slate-500 rounded"
          value={oldpass}
          onChange={changeOldpass}
        />
        {/* new password field */}
        <div className="col-start-3">New Password</div>
        <input
          type="password"
          className="col-start-5 col-span-6 border border-slate-500 rounded"
          value={newpass}
          onChange={changeNewpass}
        />
        {/* submit button */}
        <button
          className="col-start-10 bg-green-500 text-white"
          onClick={updateButtonClick}
        >
          Update User
        </button>
      </div>
    </FullPage>
  );
}
