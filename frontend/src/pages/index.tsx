import Navbar from "@components/Navbar";
import FullPage from "@components/FullPage";
import ArticlePreview from "@components/ArticlePreview";
import { CSSClass } from "@custom-types/css-props";
import { graphql } from "@gql";
import { dehydrate, QueryClient, useQuery } from "@tanstack/react-query";
import request, { GraphQLClient } from "graphql-request";
import { endpoint, avatarEndpoint } from "@request";
import { useEffect, useState } from "react";
import { ARTICLE_PAGE, TOKEN_SESSION_KEY } from "@constants";
import { GetServerSidePropsContext } from "next";
import { useRouter } from "next/router";

interface ArticleContainerProps extends CSSClass {
  contents: {
    slug: string;
    author: string;
    updatedAt: string;
    authorImage: string;
    favorited: boolean;
    favoritesCount: number;
    title: string;
    description: string;
    tags?: string[];
  }[];
}

// prettier-ignore
const articlesDocument = graphql(`query GetArticles($count: Int, $page: Int, $tags: [String!], $favorited: Boolean, $author: String) {
  getArticles(
    count: $count
    page: $page
    tags: $tags
    author: $author
    favorited: $favorited
  ) {
    articles {
      slug
      title
      description
      tagList
      updatedAt
      favorited
      favoritesCount
      author {
        username
        image
      }
    }
  }
}`)

// prettier-ignore
const tagsDocument = graphql(`query GetTags {
  getTags {
    tags
  }
}`)

async function getArticlesFn() {
  const graphqlClient = new GraphQLClient(endpoint, {
    credentials: "include",
  });
  return graphqlClient.request(articlesDocument, {});
}

async function getTagsFn() {
  return request(endpoint, tagsDocument);
}

function Banner(props: CSSClass) {
  const moreClass = props.moreClass ?? "";

  return (
    <div
      className={`flex flex-col justify-center items-center bg-green-500 shadow-inner ${moreClass}`}
    >
      <div className="flex flex-col justify-center items-center my-8">
        <h1 className="text-white text-3xl">conduit</h1>
        <p className="text-white">A place to share your knowledge</p>
      </div>
    </div>
  );
}

function ArticleContainer(props: ArticleContainerProps) {
  const moreClass = props.moreClass ?? "";

  return (
    <div className={`flex flex-col ${moreClass}`}>
      <FeedToggler />
      {props.contents.map((content) => (
        <ArticlePreview content={content} key={`container$-${content.slug}`} />
      ))}
    </div>
  );
}

function FeedToggler(props: CSSClass) {
  const moreClass = props.moreClass ?? "";

  return (
    <ul className={`flex ${moreClass}`}>
      <li className="text-green-500 p-2 border-b-2 border-green-500">
        Global Feed
      </li>
    </ul>
  );
}

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const queryClient = new QueryClient();

  await queryClient.prefetchQuery(["articles"], getArticlesFn);
  await queryClient.prefetchQuery(["tags"], getTagsFn);

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
}

function TagGroup() {
  const { isLoading, isError, data, error } = useQuery({
    queryKey: ["tags"],
    queryFn: getTagsFn,
  });

  if (isLoading) {
    return (
      <div className="outer-wrapper">
        <div
          className={`grow-0 flex flex-col gap-2 bg-slate-200 p-2 m-5 rounded col-start-5`}
        >
          <p>Popular Tags</p>
          <div className="flex flex-wrap gap-2">loading...</div>
        </div>
      </div>
    );
  }

  if (isError) {
    console.error("tagGroup: ", error);
    return (
      <div className="outer-wrapper">
        <div
          className={`grow-0 flex flex-col gap-2 bg-slate-200 p-2 m-5 rounded col-start-5`}
        >
          <p>Popular Tags</p>
          <div className="flex flex-wrap gap-2">some error occured</div>
        </div>
      </div>
    );
  }

  return (
    <div className="outer-wrapper">
      <div
        className={`grow-0 flex flex-col gap-2 bg-slate-200 p-2 m-5 rounded col-start-5`}
      >
        <p>Popular Tags</p>
        <div className="flex flex-wrap gap-2">
          {data.getTags?.tags?.map((t) => (
            <p
              key={t}
              className={`text-white whitespace-nowrap text-[0.8rem] bg-slate-400 rounded px-1`}
            >
              {t}
            </p>
          ))}
        </div>
      </div>
    </div>
  );
}

export default function Home() {
  const router = useRouter();
  const {
    isLoading: isLoadingArticles,
    isError: isErrorArticles,
    data: articlesData,
    error: articlesError,
  } = useQuery({
    queryKey: ["articles"],
    queryFn: getArticlesFn,
  });
  const [page, setPage] = useState(1);
  useEffect(() => {
    const currentPage = sessionStorage.getItem(ARTICLE_PAGE);
    if (currentPage) {
      setPage(Number(currentPage));
    } else {
      sessionStorage.setItem(ARTICLE_PAGE, "1");
      setPage(1);
    }
  }, [page]);

  if (isLoadingArticles) {
    return (
      <FullPage>
        <div className="grid grid-cols-6 gap-x-2">
          <Navbar topNavMoreClass="col-span-full" />
          <Banner moreClass="col-span-full" />
          <p className="col-start-2 col-span-3 mt-5">Loading...</p>
          <TagGroup />
        </div>
      </FullPage>
    );
  }

  if (isErrorArticles) {
    return (
      <FullPage>
        <div className="grid grid-cols-6 gap-x-2">
          <Navbar topNavMoreClass="col-span-full" />
          <Banner moreClass="col-span-full" />
          <p className="col-start-2 col-span-3 mt-5">some error occured</p>
          <TagGroup />
        </div>
      </FullPage>
    );
  }

  return (
    <FullPage>
      <div className="grid grid-cols-6 gap-x-2">
        <Navbar topNavMoreClass="col-span-full" />
        <Banner moreClass="col-span-full" />
        <ArticleContainer
          moreClass="col-start-2 col-span-3 mt-5"
          contents={
            articlesData.getArticles?.articles?.map((article) => ({
              author: article.author.username,
              authorImage: article.author.image ?? avatarEndpoint,
              description: article.description,
              favorited: article.favorited,
              favoritesCount: article.favoritesCount,
              title: article.title,
              tags: article.tagList ?? [],
              updatedAt: article.updatedAt,
              slug: article.slug,
            })) ?? []
          }
        />
        <TagGroup />
      </div>
    </FullPage>
  );
}
