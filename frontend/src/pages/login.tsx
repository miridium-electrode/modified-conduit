import Navbar from "@components/Navbar";
import Eye from "@components/icons/Eye";
import EyeSlash from "@components/icons/EyeSlash";
import { FormEvent, useState } from "react";
import { graphql } from "@gql";
import { GraphQLClient, request } from "graphql-request";
import { endpoint } from "@request";
import { useMutation } from "@tanstack/react-query";
import { useRouter } from "next/router";
import { LOGGED_IN_USERNAME, TOKEN_SESSION_KEY } from "@constants";

// prettier-ignore
const loginGraphql = graphql(`mutation Login($email: String!, $password: String!) {
  login(email: $email, password: $password) {
    username
  }
}`)

async function loginFn(payload: { email: string; password: string }) {
  const graphqlClient = new GraphQLClient(endpoint, {
    credentials: "include",
  });

  return graphqlClient.request(loginGraphql, {
    email: payload.email,
    password: payload.password,
  });
}

function LoginForm() {
  const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [togglePassword, setTogglePassword] = useState(false);
  const mutation = useMutation({
    mutationFn: loginFn,
    onSuccess: (data) => {
      sessionStorage.setItem(LOGGED_IN_USERNAME, data.login.username);
      router.replace("/");
    },
    onError: (err) => {
      console.error(err);
    },
  });

  const submit = (e: FormEvent) => {
    e.preventDefault();

    mutation.mutate({
      email,
      password,
    });
  };

  return (
    <form className="flex flex-col gap-3 w-4/5 sm:w-auto" onSubmit={submit}>
      <h1 className="text-center text-xl">Sign In</h1>
      <div className="flex flex-col gap-3">
        <div className="flex flex-col">
          <label htmlFor="email">Email</label>
          <input
            type="email"
            className="border border-black"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className="flex flex-col">
          <label htmlFor="password">Password</label>
          <div className="flex">
            {togglePassword ? (
              <>
                <input
                  type="text"
                  className="border border-black flex-grow"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                <button className="bg-slate-300" type="button">
                  <EyeSlash onClick={() => setTogglePassword(false)} />
                </button>
              </>
            ) : (
              <>
                <input
                  type="password"
                  className="border border-black flex-grow"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                <button className="bg-slate-300" type="button">
                  <Eye onClick={() => setTogglePassword(true)} />
                </button>
              </>
            )}
          </div>
        </div>
        <button className="bg-green-700 text-white" type="submit">
          Sign In
        </button>
      </div>
    </form>
  );
}

function FormContainer() {
  return (
    <div className="flex justify-center">
      <LoginForm />
    </div>
  );
}

export default function Login() {
  return (
    <div className="flex flex-col h-screen">
      <Navbar />
      <FormContainer />
    </div>
  );
}
