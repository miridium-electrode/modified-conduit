import Navbar from "@components/Navbar";
import { graphql } from "@gql";
import { request } from "graphql-request";
import { endpoint } from "@request";
import { FormEvent, useState } from "react";
import Eye from "@components/icons/Eye";
import EyeSlash from "@components/icons/EyeSlash";
import { useMutation } from "@tanstack/react-query";
import { useRouter } from "next/router";

// prettier-ignore
const registerGraphql = graphql(`mutation Register($username: String!, $email: String!, $password: String!, $repass: String!) {
  register(
    username: $username
    email: $email
    password: $password
    repass: $repass
  ) {
    token
  }
}`);

async function registerFn(payload: {
  username: string;
  email: string;
  password: string;
  retypePassword: string;
}) {
  return request(endpoint, registerGraphql, {
    email: payload.email,
    password: payload.password,
    repass: payload.retypePassword,
    username: payload.username,
  });
}

function RegisterForm() {
  const router = useRouter();
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [retypePassword, setRetypePassword] = useState("");
  const [passwordSee, setPasswordSee] = useState(false);
  const [retypePasswordSee, setRetypePasswordSee] = useState(false);
  const mutation = useMutation({
    mutationFn: registerFn,
    onSuccess: () => {
      router.replace("/login");
    },
    onError: (error) => {
      console.error(error);
    },
  });

  const submit = (e: FormEvent) => {
    e.preventDefault();
    mutation.mutate({
      username,
      email,
      password,
      retypePassword,
    });
  };

  return (
    <form onSubmit={submit} className="w-4/5 sm:w-auto">
      <div className="flex flex-col gap-3">
        <h1 className="text-center">Sign Up</h1>
        <div className="flex flex-col gap-3">
          <div className="flex flex-col">
            <label htmlFor="username">Username</label>
            <input
              type="text"
              className="border border-black"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div className="flex flex-col">
            <label htmlFor="email">Email</label>
            <input
              type="email"
              className="border border-black"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="flex flex-col">
            <label htmlFor="password">Password</label>
            <div className="flex">
              {passwordSee ? (
                <>
                  <input
                    type="text"
                    className="border border-black flex-grow"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                  <button className="bg-slate-400" type="button">
                    <EyeSlash onClick={() => setPasswordSee(false)} />
                  </button>
                </>
              ) : (
                <>
                  <input
                    type="password"
                    className="border border-black flex-grow"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                  <button className="bg-slate-400" type="button">
                    <Eye onClick={() => setPasswordSee(true)} />
                  </button>
                </>
              )}
            </div>
          </div>
          <div className="flex flex-col">
            <label htmlFor="password">Retype Password</label>
            <div className="flex">
              {retypePasswordSee ? (
                <>
                  <input
                    type="text"
                    className="border border-black flex-grow"
                    value={retypePassword}
                    onChange={(e) => setRetypePassword(e.target.value)}
                  />
                  <button className="bg-slate-400" type="button">
                    <EyeSlash onClick={() => setRetypePasswordSee(false)} />
                  </button>
                </>
              ) : (
                <>
                  <input
                    type="password"
                    className="border border-black flex-grow"
                    value={retypePassword}
                    onChange={(e) => setRetypePassword(e.target.value)}
                  />
                  <button className="bg-slate-400" type="button">
                    <Eye onClick={() => setRetypePasswordSee(true)} />
                  </button>
                </>
              )}
            </div>
          </div>
          <button className="bg-green-700 text-white" type="submit">
            Sign Up
          </button>
        </div>
      </div>
    </form>
  );
}

function FormContainer() {
  return (
    <div className="flex justify-center">
      <RegisterForm />
    </div>
  );
}

export default function Register() {
  return (
    <div className="flex flex-col h-screen">
      <Navbar />
      <FormContainer />
    </div>
  );
}
