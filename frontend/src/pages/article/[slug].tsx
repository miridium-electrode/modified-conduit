import FullPage from "@components/FullPage";
import { useRouter } from "next/router";
import Navbar from "@components/Navbar";
import { CSSClass } from "@custom-types/css-props";
import { graphql } from "@gql";
import {
  dehydrate,
  QueryClient,
  useMutation,
  useQuery,
  useQueryClient,
} from "@tanstack/react-query";
import request, { GraphQLClient } from "graphql-request";
import { endpoint, avatarEndpoint } from "@request";
import Image from "next/image";
import { GetServerSidePropsContext } from "next";
import FavoriteButton from "@components/FavoriteButton";
import { TOKEN_SESSION_KEY } from "@constants";
import PlusIcon from "@components/icons/Plus";
import { useIsLoggedIn } from "@hooks/login";

// prettier-ignore
const articleDocument = graphql(`query GetArticleBySlug($slug: String!) {
  getArticleBySlug(slug: $slug) {
    title
    description
    body
    tagList
    updatedAt
    favorited
    favoritesCount
    author {
      following
      username
      bio
      image
    }
  }
}`);

// prettier-ignore
const followUserDocument = graphql(`mutation FollowUser($username: String!) {
  followUser(username: $username) {
    following
  }
}`)

// prettier-ignore
const unfollowUserDocument = graphql(`mutation unfollowUser($username: String!) {
  unfollowUser(username: $username) {
    following
  }
}`)

async function getArticleBySlug(slug: string) {
  if (typeof window === "object") {
    const token = sessionStorage.getItem(TOKEN_SESSION_KEY);
    if (token) {
      const graphqlClient = new GraphQLClient(endpoint, {
        headers: {
          authorization: `Bearer ${token}`,
        },
      });

      return graphqlClient.request(articleDocument, {
        slug,
      });
    }
    return request(endpoint, articleDocument, {
      slug,
    });
  }

  return request(endpoint, articleDocument, {
    slug,
  });
}

async function follow(username: string) {
  const graphqlClient = new GraphQLClient(endpoint, {
    credentials: "include",
  });

  return graphqlClient.request(followUserDocument, {
    username,
  });
}

async function unfollow(username: string) {
  const graphqlClient = new GraphQLClient(endpoint, {
    credentials: "include",
  });

  return graphqlClient.request(unfollowUserDocument, {
    username,
  });
}

interface AuthorProps extends CSSClass {
  image: string;
  name: string;
  updatedAt: string;
}

function AuthorProfile(props: AuthorProps) {
  return (
    <div className={`flex col-start-3 col-span-2 ${props.moreClass ?? ""}`}>
      <Image
        src={props.image}
        width={40}
        height={40}
        alt={`${props.name} picture`}
      />
      <div className="flex flex-col">
        <p>{props.name}</p>
        <p className="text-xs text-slate-400">{props.updatedAt}</p>
      </div>
    </div>
  );
}

type FollowButtonProps = {
  following: boolean;
  username: string;
  slug: string;
};

function FollowButton(props: FollowButtonProps) {
  const router = useRouter();
  const loggedIn = useIsLoggedIn();
  const queryClient = useQueryClient();
  const followMutation = useMutation({
    mutationFn: (username: string) => follow(username),
  });
  const unfollowMutation = useMutation({
    mutationFn: (username: string) => unfollow(username),
  });

  const click = () => {
    if (!loggedIn) {
      router.replace("/login");
    }

    if (props.following) {
      // unfollow
      unfollowMutation.mutate(props.username, {
        onSuccess(data, variables, context) {
          queryClient.invalidateQueries({ queryKey: ["article", props.slug] });
        },
      });
    } else {
      // follow
      followMutation.mutate(props.username, {
        onSuccess(data, variables, context) {
          queryClient.invalidateQueries({ queryKey: ["article", props.slug] });
        },
      });
    }
  };

  if (props.following) {
    return (
      <div
        className="col-span-2 flex justify-center items-center"
        onClick={click}
      >
        <div className="border rounded border-slate-500 flex items-center p-1 hover:cursor-pointer">
          <PlusIcon className="w-4 h-4" />
          <p>Unfollow {props.username}</p>
        </div>
      </div>
    );
  }

  return (
    <div
      className="col-span-2 flex justify-center items-center"
      onClick={click}
    >
      <div className="border rounded border-slate-500 flex items-center p-1 hover:cursor-pointer">
        <PlusIcon className="w-4 h-4" />
        <p>follow {props.username}</p>
      </div>
    </div>
  );
}

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const queryClient = new QueryClient();
  const slug = context.params?.slug as string;

  await queryClient.prefetchQuery(["article", slug], () =>
    getArticleBySlug(slug)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
}

export default function SingleArticle() {
  const router = useRouter();
  const slug = router.query?.slug as string;
  const { isLoading, isError, data, error } = useQuery({
    queryKey: ["article", slug],
    queryFn: () => getArticleBySlug(slug),
  });

  if (isLoading) {
    return (
      <FullPage>
        <div className="grid grid-cols-12 gap-x-6">
          <Navbar topNavMoreClass="col-span-full" />
          <div className="col-start-3">loading...</div>
        </div>
      </FullPage>
    );
  }

  if (isError) {
    console.error(error);
    return (
      <FullPage>
        <div className="grid grid-cols-12 gap-x-6">
          <Navbar topNavMoreClass="col-span-full" />
          <div className="col-start-3">an error occured</div>
        </div>
      </FullPage>
    );
  }

  return (
    <FullPage>
      <div className="grid grid-cols-12 gap-x-6">
        <Navbar topNavMoreClass="col-span-full" />
        <h1 className="text-3xl col-start-3 col-span-8 justify-self-center pt-10">
          {data.getArticleBySlug.title}
        </h1>
        <p className="text-slate-400 col-start-3 col-span-8 justify-self-center">
          {data.getArticleBySlug.description}
        </p>
        <AuthorProfile
          image={data.getArticleBySlug.author.image ?? avatarEndpoint}
          name={data.getArticleBySlug.author.username}
          updatedAt={data.getArticleBySlug.updatedAt}
          moreClass="py-6"
        />
        <FavoriteButton
          slug={slug}
          favorited={data.getArticleBySlug.favorited}
          favoritesCount={data.getArticleBySlug.favoritesCount}
        />
        <FollowButton
          slug={slug}
          following={data.getArticleBySlug.author.following}
          username={data.getArticleBySlug.author.username}
        />
        <p className="col-start-3 col-span-8 justify-self-center">
          {data.getArticleBySlug.body}
        </p>
      </div>
    </FullPage>
  );
}
