import ArticlePreview from "@components/ArticlePreview";
import FullPage from "@components/FullPage";
import PlusIcon from "@components/icons/Plus";
import Navbar from "@components/Navbar";
import { TOKEN_SESSION_KEY } from "@constants";
import { graphql } from "@gql";
import { useIsLoggedIn } from "@hooks/login";
import { avatarEndpoint, endpoint } from "@request";
import {
  dehydrate,
  QueryClient,
  useMutation,
  useQuery,
  useQueryClient,
} from "@tanstack/react-query";
import request, { GraphQLClient } from "graphql-request";
import { GetServerSidePropsContext } from "next";
import Image from "next/image";
import { useRouter } from "next/router";

// prettier-ignore
const profileDocument = graphql(`query GetProfile($username: String!) {
  getProfile(username: $username) {
    username
    image
    following
  }
}`)

// prettier-ignore
const articlesDocument = graphql(`query GetArticles($count: Int, $page: Int, $tags: [String!], $favorited: Boolean, $author: String) {
  getArticles(
    count: $count
    page: $page
    tags: $tags
    author: $author
    favorited: $favorited
  ) {
    articles {
      slug
      title
      description
      tagList
      updatedAt
      favorited
      favoritesCount
      author {
        username
        image
      }
    }
  }
}`)

// prettier-ignore
const followUserDocument = graphql(`mutation FollowUser($username: String!) {
  followUser(username: $username) {
    following
  }
}`)

// prettier-ignore
const unfollowUserDocument = graphql(`mutation unfollowUser($username: String!) {
  unfollowUser(username: $username) {
    following
  }
}`)

async function getProfile(username: string) {
  const graphqlClient = new GraphQLClient(endpoint, {
    credentials: "include",
  });
  return graphqlClient.request(profileDocument, {
    username,
  });
}

async function getArticlesByUsername(username: string) {
  const graphqlClient = new GraphQLClient(endpoint, {
    credentials: "include",
  });

  return graphqlClient.request(articlesDocument, {
    author: username,
  });
}

async function follow(username: string) {
  const graphqlClient = new GraphQLClient(endpoint, {
    credentials: "include",
  });

  return graphqlClient.request(followUserDocument, {
    username,
  });
}

async function unfollow(username: string) {
  const graphqlClient = new GraphQLClient(endpoint, {
    credentials: "include",
  });

  return graphqlClient.request(unfollowUserDocument, {
    username,
  });
}

type FollowButtonProps = {
  following: boolean;
  username: string;
};

function FollowButton(props: FollowButtonProps) {
  const router = useRouter();
  const queryClient = useQueryClient();
  const loggedIn = useIsLoggedIn();
  const followMutation = useMutation({
    mutationFn: (username: string) => follow(username),
  });
  const unfollowMutation = useMutation({
    mutationFn: (username: string) => unfollow(username),
  });

  const click = () => {
    if (!loggedIn) {
      router.replace("/login");
    }

    if (props.following) {
      // unfollow
      unfollowMutation.mutate(props.username, {
        onSuccess(data, variables, context) {
          queryClient.invalidateQueries({
            queryKey: ["profile", props.username],
          });
        },
      });
    } else {
      // follow
      followMutation.mutate(props.username, {
        onSuccess(data, variables, context) {
          queryClient.invalidateQueries({
            queryKey: ["profile", props.username],
          });
        },
      });
    }
  };

  if (props.following) {
    return (
      <div className="flex justify-center items-center" onClick={click}>
        <div className="border rounded border-slate-500 flex items-center p-1 hover:cursor-pointer">
          <PlusIcon className="w-4 h-4" />
          <p>Unfollow {props.username}</p>
        </div>
      </div>
    );
  }

  return (
    <div className="flex justify-center items-center" onClick={click}>
      <div className="border rounded border-slate-500 flex items-center p-1 hover:cursor-pointer">
        <PlusIcon className="w-4 h-4" />
        <p>follow {props.username}</p>
      </div>
    </div>
  );
}

function AuthorDefinition() {
  const router = useRouter();
  const username = router.query?.username as string;
  const profileQuery = useQuery({
    queryKey: ["profile", username],
    queryFn: async () => getProfile(username),
  });

  if (profileQuery.isLoading) {
    return <p>Loading...</p>;
  }

  if (profileQuery.isError) {
    console.error(profileQuery.error);
    return <p>Some error occured</p>;
  }

  return (
    <div className="col-span-full flex justify-center items-center bg-slate-200">
      <div className="flex flex-col my-6 items-center">
        <Image
          src={profileQuery.data.getProfile.image ?? avatarEndpoint}
          alt={profileQuery.data.getProfile.username}
          width={100}
          height={100}
        />
        <p>{profileQuery.data.getProfile.username}</p>
        <FollowButton
          following={profileQuery.data.getProfile.following}
          username={profileQuery.data.getProfile.username}
        />
      </div>
    </div>
  );
}

function AuthorArticles() {
  const router = useRouter();
  const username = router.query?.username as string;
  const articlesQuery = useQuery({
    queryKey: ["article", username],
    queryFn: async () => getArticlesByUsername(username),
  });

  if (articlesQuery.isLoading) {
    return <p>Loading...</p>;
  }

  if (articlesQuery.isError) {
    console.error(articlesQuery.error);
    return <p>Some error occured</p>;
  }

  return (
    <div className="col-start-3 col-span-8 flex flex-col mt-3">
      {articlesQuery.data.getArticles.articles?.map((article) => (
        <ArticlePreview
          key={article.slug}
          content={{
            author: article.author.username,
            authorImage: article.author.image ?? avatarEndpoint,
            description: article.description,
            favorited: article.favorited ?? false,
            favoritesCount: article.favoritesCount ?? 0,
            slug: article.slug,
            title: article.title,
            updatedAt: article.updatedAt,
            tags: article.tagList ?? [],
          }}
        />
      ))}
    </div>
  );
}

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const username = context.params?.username as string;
  const queryClient = new QueryClient();

  await queryClient.prefetchQuery(["profile", username], async () =>
    getProfile(username)
  );
  await queryClient.prefetchQuery(["article", username], async () =>
    getArticlesByUsername(username)
  );

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  };
}

export default function Profile() {
  return (
    <FullPage>
      <div className="grid grid-cols-12">
        <Navbar topNavMoreClass="col-span-full" />
        <AuthorDefinition />
        <AuthorArticles />
      </div>
    </FullPage>
  );
}
