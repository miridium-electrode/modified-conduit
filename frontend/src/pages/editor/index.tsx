import { ChangeEvent, FormEvent, ReactNode, useEffect, useState } from "react";
import FullPage from "@components/FullPage";
import { useRouter } from "next/router";
import { graphql } from "@gql";
import { GraphQLClient } from "graphql-request";
import { endpoint } from "@request";
import { useMutation } from "@tanstack/react-query";
import Navbar from "@components/Navbar";
import { ResponseError } from "@custom-types/api-response";
import { useAuthentication } from "@hooks/authentication";

type ComponentWithChild = { children: ReactNode };

function FormWrapper({ children }: ComponentWithChild) {
  return (
    <div className="flex flex-col justify-center items-center">{children}</div>
  );
}

function FlexWrapper({ children }: ComponentWithChild) {
  return <div className="flex flex-col">{children}</div>;
}

// prettier-ignore
const createArticleGql = graphql(`mutation CreateArticle($title: String!, $description: String!, $body: String!, $tagList: [String!]) {
  createArticle(
    title: $title
    description: $description
    body: $body
    tagList: $tagList
  ) {
    slug
    title
    description
    body
    tagList
    createdAt
    updatedAt
    author {
      username
      image
    }
  }
}`)

async function CreateArticleFn(payload: {
  title: string;
  description: string;
  body: string;
  tagString: string;
}) {
  const tagList = payload.tagString.split(" ");
  const graphqlClient = new GraphQLClient(endpoint, {
    credentials: "include",
  });

  return graphqlClient.request(createArticleGql, {
    body: payload.body,
    description: payload.description,
    title: payload.title,
    tagList,
  });
}

export default function () {
  const router = useRouter();
  useAuthentication();
  const [title, setTitle] = useState("");
  const [desc, setDesc] = useState("");
  const [body, setBody] = useState("");
  const [tags, setTags] = useState("");
  const mutation = useMutation({
    mutationFn: CreateArticleFn,
    onSuccess: (data) => {
      console.log(data);
    },
    onError(err) {
      const response = (err as any).response as ResponseError;
      if (response.errors[0].message === "Unauthorized") {
        router.replace("/login");
      } else {
        console.error(err);
      }
    },
  });

  const changeTitle = (e: ChangeEvent<HTMLInputElement>) =>
    setTitle(e.target.value);
  const changeDesc = (e: ChangeEvent<HTMLInputElement>) =>
    setDesc(e.target.value);
  const changeBody = (e: ChangeEvent<HTMLTextAreaElement>) =>
    setBody(e.target.value);
  const changeTags = (e: ChangeEvent<HTMLInputElement>) =>
    setTags(e.target.value);

  const createNewArticle = (e: FormEvent) => {
    e.preventDefault();

    mutation.mutate({
      body,
      title,
      description: desc,
      tagString: tags,
    });
  };

  return (
    <FullPage>
      <FlexWrapper>
        <Navbar />
        <FormWrapper>
          <form onSubmit={createNewArticle} className="flex flex-col w-4/5">
            <label htmlFor="title">Title</label>
            <input
              type="text"
              name="title"
              className="border border-black"
              onChange={changeTitle}
              value={title}
            />
            <label htmlFor="description">Description</label>
            <input
              type="text"
              name="description"
              className="border border-black"
              onChange={changeDesc}
              value={desc}
            />
            <label htmlFor="body">Body</label>
            <textarea
              name="body"
              className="border border-black"
              onChange={changeBody}
              value={body}
            ></textarea>
            <label htmlFor="tags">Tags</label>
            <input
              type="text"
              name="tags"
              className="border border-black"
              onChange={changeTags}
              value={tags}
            />
            <button type="submit" className="p-2 bg-green-500 text-white">
              Publish Article
            </button>
          </form>
        </FormWrapper>
      </FlexWrapper>
    </FullPage>
  );
}
