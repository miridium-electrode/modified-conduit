export type ResponseError = {
  errors: {
    message: string;
    extensions: {
      code: string;
      response: {
        statusCode: number;
        message: string;
      };
    };
  }[];
  data: null;
};
