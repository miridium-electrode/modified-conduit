export const TAGS_FILTER_SESSION_KEY = "tagsFilter";
export const TOKEN_SESSION_KEY = "token";
export const LOGGED_IN_USERNAME = "username";
export const ARTICLE_PAGE = "page";
