interface TagProps {
  content: string;
}

export function TagOutline(props: TagProps) {
  return (
    <p
      className={`text-slate-400 whitespace-nowrap text-[0.8rem] border border-slate-400 rounded px-1`}
    >
      {props.content}
    </p>
  );
}

export default function Tag(props: TagProps) {
  return (
    <p
      className={`text-white whitespace-nowrap text-[0.8rem] bg-slate-400 rounded px-1`}
    >
      {props.content}
    </p>
  );
}
