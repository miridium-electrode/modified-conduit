import { TOKEN_SESSION_KEY } from "@constants";
import { graphql } from "@gql";
import { useIsLoggedIn } from "@hooks/login";
import { endpoint } from "@request";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { GraphQLClient } from "graphql-request";
import { useRouter } from "next/router";
import Heart from "./icons/Heart";

type FavoriteButtonProps = {
  slug: string;
  favoritesCount: number;
  favorited: boolean;
};

// prettier-ignore
const favoriteDocument = graphql(`mutation FavoriteArticle($slug: String!) {
  favoriteArticle(slug: $slug) {
    favorited
    favoritesCount
  }
}`);

// prettier-ignore
const unfavoriteDocument = graphql(`mutation UnfavoriteArticle($slug: String!) {
  unfavoriteArticle(slug: $slug) {
    favorited
    favoritesCount
  }
}`)

async function favorite(slug: string) {
  let token = sessionStorage.getItem(TOKEN_SESSION_KEY);

  const graphqlClient = new GraphQLClient(endpoint, {
    headers: {
      authorization: `Bearer ${token}`,
    },
  });

  return graphqlClient.request(favoriteDocument, {
    slug,
  });
}

async function unfavorite(slug: string) {
  let token = sessionStorage.getItem(TOKEN_SESSION_KEY);

  const graphqlClient = new GraphQLClient(endpoint, {
    headers: {
      authorization: `Bearer ${token}`,
    },
  });

  return graphqlClient.request(unfavoriteDocument, {
    slug,
  });
}

export default function FavoriteButton(props: FavoriteButtonProps) {
  const router = useRouter();
  const loggedId = useIsLoggedIn();
  const favoriteMutation = useMutation({
    mutationFn: (slug: string) => favorite(slug),
  });
  const queryClient = useQueryClient();
  const unfavoriteMutation = useMutation({
    mutationFn: (slug: string) => unfavorite(slug),
  });

  const favoriteButton = () => {
    if (!loggedId) {
      router.replace("/login");
      return;
    }

    if (props.favorited) {
      console.log("disliked");
      unfavoriteMutation.mutate(props.slug, {
        onSuccess: () => {
          queryClient.invalidateQueries({
            queryKey: ["article", props.slug],
          });
          queryClient.invalidateQueries({
            queryKey: ["articles"],
          });
        },
      });
    } else {
      console.log("liked");
      favoriteMutation.mutate(props.slug, {
        onSuccess: () => {
          queryClient.invalidateQueries({
            queryKey: ["article", props.slug],
          });
          queryClient.invalidateQueries({
            queryKey: ["articles"],
          });
        },
      });
    }
  };

  if (props.favorited) {
    return (
      <div className="flex justify-center items-center">
        <div
          className="flex border border-green-500 bg-green-500 rounded-md justify-center items-center mx-2 my-2 px-2"
          onClick={favoriteButton}
        >
          <Heart className="w-4 h-4" fill="#ffffff" stroke="#ffffff" />
          <span className="text-sm text-white">{props.favoritesCount}</span>
        </div>
      </div>
    );
  }

  return (
    <div className="flex justify-center items-center">
      <div
        className="flex border border-green-500 rounded-md justify-center items-center mx-2 my-2 px-2"
        onClick={favoriteButton}
      >
        <Heart className="w-4 h-4" />
        <span className="text-sm text-green-500">{props.favoritesCount}</span>
      </div>
    </div>
  );
}
