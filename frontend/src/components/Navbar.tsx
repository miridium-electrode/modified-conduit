import Link from "next/link";
import Menu from "@components/icons/Menu";
import Search from "@components/icons/Search";
import X from "@components/icons/X";
import Settings from "./icons/Settings";
import {
  createContext,
  Dispatch,
  SetStateAction,
  useContext,
  useEffect,
  useState,
} from "react";
import { useIsLoggedIn } from "@hooks/login";
import { useSessionUsername } from "@hooks/username";
import { graphql } from "@gql";
import { useQuery } from "@tanstack/react-query";
import request, { GraphQLClient } from "graphql-request";
import { avatarEndpoint, endpoint } from "@request";
import Image from "next/image";

interface TopNavProps {
  topNavMoreClass?: string;
}

interface NavProps extends TopNavProps {}

// prettier-ignore
const profilePicDocument = graphql(`query GetProfileImage($username: String!) {
  getProfile(username: $username) {
    image
  }
}`)

async function getProfilePicByUsername(username: string) {
  const graphqlClient = new GraphQLClient(endpoint, {
    credentials: "include",
  });

  return graphqlClient.request(profilePicDocument, {
    username,
  });
}

const NavSlider = createContext<[boolean, Dispatch<SetStateAction<boolean>>]>([
  false,
  () => {},
]);

function useProfilePicGetter() {
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [error, setError] = useState<unknown>();
  const [data, setData] = useState("");
  const username = useSessionUsername();

  useEffect(() => {
    if (username) {
      getProfilePicByUsername(username)
        .then((profileQuery) => {
          setIsLoading(false);
          setData(profileQuery.getProfile.image ?? avatarEndpoint);
        })
        .catch((e) => {
          setIsLoading(false);
          setIsError(true);
          setError(e);
        });
    }
  }, [username, isLoading]);

  return { isLoading, isError, error, data };
}

function NavWithSearch(props: TopNavProps) {
  const moreClass = props.topNavMoreClass ?? "";
  const [_, setSidenav] = useContext(NavSlider);

  return (
    <div className={`flex flex-row justify-between items-center ${moreClass}`}>
      <div className="flex flex-row items-center gap-2 m-2 sm:ml-4 sm:mt-0 sm:mb-0 sm:mr-0">
        <Menu className="h-6 w-6 sm:hidden" onClick={() => setSidenav(true)} />
        <Link href="/" className="font-bold text-green-700 text-lg">
          conduit
        </Link>
      </div>
      <nav className="hidden gap-3 mr-4 sm:flex sm:flex-row sm:items-center">
        <div className="flex flex-row">
          <input type="text" className="border-solid border-2 border-black" />
          <button className="bg-slate-300">
            <Search />
          </button>
        </div>
        <Link href="/">Home</Link>
        <Link href="/login">Sign In</Link>
        <Link href="/register">Sign Up</Link>
      </nav>
    </div>
  );
}

function SideNav() {
  const [sidenav, setSidenav] = useContext(NavSlider);

  let sidenavAppear = "-translate-x-full";

  if (sidenav) {
    sidenavAppear = "translate-x-0";
  } else {
    sidenavAppear = "-translate-x-full";
  }

  return (
    <div
      className={`absolute h-screen bg-slate-500 
            text-white flex flex-col p-4 gap-2 transition-transform ${sidenavAppear}`}
    >
      <X className="absolute top-0 right-0" onClick={() => setSidenav(false)} />
      <Link href="/">Home</Link>
      <Link href="/login">Sign In</Link>
      <Link href="/register">Sign Up</Link>
    </div>
  );
}

function AuthSideNav() {
  const [sidenav, setSidenav] = useContext(NavSlider);
  const [username, setUsername] = useState("");

  useEffect(() => {
    const localUsername = sessionStorage.getItem("username");
    if (localUsername) {
      setUsername(localUsername);
    }
  }, []);

  let sidenavAppear = "-translate-x-full";

  if (sidenav) {
    sidenavAppear = "translate-x-0";
  } else {
    sidenavAppear = "-translate-x-full";
  }

  return (
    <div
      className={`absolute h-screen bg-slate-500 
            text-white flex flex-col p-4 gap-2 transition-transform ${sidenavAppear}`}
    >
      <X className="absolute top-0 right-0" onClick={() => setSidenav(false)} />
      <Link href="/">Home</Link>
      <Link href="/editor">New Article</Link>
      <Link href="/settings">Settings</Link>
      <Link href={`/${username}`}>{username}</Link>
    </div>
  );
}

function AuthNavWithSearch(props: TopNavProps) {
  const moreClass = props.topNavMoreClass ?? "";
  const [_, setSidenav] = useContext(NavSlider);
  const { data, error, isError, isLoading } = useProfilePicGetter();
  const username = useSessionUsername();

  if (isLoading) {
    return (
      <div className={`flex justify-between items-center ${moreClass}`}>
        <div className="flex flex-row items-center gap-2 m-2 sm:ml-4 sm:mt-0 sm:mb-0 sm:mr-0">
          <Menu
            className="h-6 w-6 sm:hidden"
            onClick={() => setSidenav(true)}
          />
          <Link href="/" className="font-bold text-green-700 text-lg">
            conduit
          </Link>
        </div>
        <nav className="hidden gap-3 mr-4 sm:flex sm:flex-row sm:items-center">
          <div className="flex flex-row">
            <input type="text" className="border-solid border-2 border-black" />
            <button className="bg-slate-300">
              <Search />
            </button>
          </div>
          <Link href="/">Home</Link>
          <Link href="/editor">New Article</Link>
          <Link href="/settings">
            <div className="flex items-center">
              <Settings className="w-4 h-4" />
              <p>Settings</p>
            </div>
          </Link>
          <Link href={`/${username}`}>{username}</Link>
        </nav>
      </div>
    );
  }

  if (isError) {
    console.error(error);
    return (
      <div className={`flex justify-between items-center ${moreClass}`}>
        <div className="flex flex-row items-center gap-2 m-2 sm:ml-4 sm:mt-0 sm:mb-0 sm:mr-0">
          <Menu
            className="h-6 w-6 sm:hidden"
            onClick={() => setSidenav(true)}
          />
          <Link href="/" className="font-bold text-green-700 text-lg">
            conduit
          </Link>
        </div>
        <nav className="hidden gap-3 mr-4 sm:flex sm:flex-row sm:items-center">
          <div className="flex flex-row">
            <input type="text" className="border-solid border-2 border-black" />
            <button className="bg-slate-300">
              <Search />
            </button>
          </div>
          <Link href="/">Home</Link>
          <Link href="/editor">New Article</Link>
          <Link href="/settings">
            <div className="flex items-center">
              <Settings className="w-4 h-4" />
              <p>Settings</p>
            </div>
          </Link>
          <Link href={`/${username}`}>{username}</Link>
        </nav>
      </div>
    );
  }

  return (
    <div className={`flex justify-between items-center ${moreClass}`}>
      <div className="flex flex-row items-center gap-2 m-2 sm:ml-4 sm:mt-0 sm:mb-0 sm:mr-0">
        <Menu className="h-6 w-6 sm:hidden" onClick={() => setSidenav(true)} />
        <Link href="/" className="font-bold text-green-700 text-lg">
          conduit
        </Link>
      </div>
      <nav className="hidden gap-3 mr-4 sm:flex sm:flex-row sm:items-center">
        <div className="flex flex-row">
          <input type="text" className="border-solid border-2 border-black" />
          <button className="bg-slate-300">
            <Search />
          </button>
        </div>
        <Link href="/">Home</Link>
        <Link href="/editor">New Article</Link>
        <Link href="/settings">
          <div className="flex items-center">
            <Settings className="w-4 h-4" />
            <p>Settings</p>
          </div>
        </Link>
        <Link href={`/${username}`}>
          <div className="flex items-center gap-1">
            <Image src={data} alt={""} width={15} height={15} />
            <p>{username}</p>
          </div>
        </Link>
      </nav>
    </div>
  );
}

export default function Navbar(props: NavProps) {
  const navTrigger = useState(false);
  const loggedIn = useIsLoggedIn();

  if (loggedIn) {
    return (
      <NavSlider.Provider value={navTrigger}>
        <AuthSideNav />
        <AuthNavWithSearch topNavMoreClass={props.topNavMoreClass} />
      </NavSlider.Provider>
    );
  }

  return (
    <NavSlider.Provider value={navTrigger}>
      <SideNav />
      <NavWithSearch topNavMoreClass={props.topNavMoreClass} />
    </NavSlider.Provider>
  );
}
