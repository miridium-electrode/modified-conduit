import { CSSClass } from "@custom-types/css-props";
import Tag from "./Tag";

interface TagGroupProps extends CSSClass {
  tags?: string[];
}

export default function TagGroup(props: TagGroupProps) {
  const moreClass = props.moreClass ?? "";
  const tags =
    props.tags !== undefined
      ? props.tags.map((tag) => <Tag content={tag} key={tag} />)
      : null;

  return (
    <div className="outer-wrapper">
      <div
        className={`grow-0 flex flex-col gap-2 bg-slate-200 p-2 m-5 rounded ${moreClass}`}
      >
        <p>Popular Tags</p>
        <div className="flex flex-wrap gap-2">{tags}</div>
      </div>
    </div>
  );
}
