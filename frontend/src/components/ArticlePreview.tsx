import Image from "next/image";
import Heart from "./icons/Heart";
import Link from "next/link";
import { TagOutline } from "./Tag";
import { useIsLoggedIn } from "@hooks/login";
import FavoriteButton from "./FavoriteButton";

type ArticlePreviewProps = {
  content: {
    slug: string;
    author: string;
    updatedAt: string;
    authorImage: string;
    favorited: boolean;
    favoritesCount: number;
    title: string;
    description: string;
    tags?: string[];
  };
};

type ProfileProps = {
  author: {
    image: string;
    name: string;
    updatedAt: string;
  };
};

function Profile(props: ProfileProps) {
  return (
    <div className="flex gap-1 items-center">
      <Image
        src={props.author.image}
        width={25}
        height={25}
        alt={`picture of ${props.author.name}`}
      />
      <div className="flex flex-col">
        <Link href={`/${props.author.name}`}>
          <p className="text-green-500">{props.author.name}</p>
        </Link>
        <p className="text-xs text-slate-400">{props.author.updatedAt}</p>
      </div>
    </div>
  );
}

export default function ArticlePreview(props: ArticlePreviewProps) {
  return (
    <div className="flex flex-col py-2 border-t-[1px] border-t-article-preview-top">
      <div className="flex justify-between">
        <Profile
          author={{
            image: props.content.authorImage,
            name: props.content.author,
            updatedAt: props.content.updatedAt,
          }}
        />
        <FavoriteButton
          slug={props.content.slug}
          favorited={props.content.favorited}
          favoritesCount={props.content.favoritesCount}
        />
      </div>
      <Link href={`/article/${props.content.slug}`}>
        <h2 className="text-2xl">{props.content.title}</h2>
      </Link>
      <Link href={`/article/${props.content.slug}`}>
        <p className="text-gray-400 mb-3">{props.content.description}</p>
      </Link>
      <div className="flex justify-between">
        <Link href={`/article/${props.content.slug}`}>
          <p className="text-gray-300 text-sm">read more...</p>
        </Link>
        <div className="flex flex-wrap gap-1">
          {props.content.tags?.map((tag) => (
            <TagOutline key={tag} content={tag} />
          ))}
        </div>
      </div>
    </div>
  );
}
